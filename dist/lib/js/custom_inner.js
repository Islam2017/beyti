function openNav() {
    document.getElementById("open").style.display = "none";
    document.getElementById("nav").style.width = "100%";
}

function closeNav() {
    document.getElementById("nav").style.width = "0";
    document.getElementById("open").style.display = "block";
}

// start header2 scrolling nav
jQuery(document).ready(function() {
    var previousScroll = 0,
        headerOrgOffset = jQuery('.head-pages').offset().top;
    
    jQuery('#header-wrap').height(jQuery('.head-pages').height());

    jQuery(window).scroll(function() {
        var currentScroll = jQuery(this).scrollTop();
        console.log(currentScroll + " and " + previousScroll + " and " + headerOrgOffset);
        if(currentScroll > headerOrgOffset) {
            if (currentScroll > previousScroll) {
                jQuery('.head-pages').fadeOut();
            } else {
                jQuery('.head-pages').fadeIn();
                jQuery('.sticker').addClass('is-sticky');
            }
        } else {
             jQuery('.sticker').removeClass('is-sticky');   
        }
        previousScroll = currentScroll;
    });
    
});

// show more button 

$( document ).ready(function () {
		$(".moreBox").slice(0, 3).show();
		if ($(".blogBox:hidden").length != 0) {
			$("#loadMore").show();
		}		
		$("#loadMore").on('click', function (e) {
			e.preventDefault();
			$(".moreBox:hidden").slice(0, 6).slideDown();
			if ($(".moreBox:hidden").length == 0) {
				$("#loadMore").fadeOut('slow');
			}
		});
	});

$('#news-slideShow').owlCarousel({
    rtl:true,
    loop:true, 
    nav:true,
    items: 1
});
$(".gallery .owl-prev").html('<i class="icon-play-button-1"></i>');
$(".gallery .owl-next").html('<i class="icon-play-button-1"></i>');