
    <?php include "include/header.php" ?>
       
       <!-- Start slider header -->
       
        <div class="home-slider">
            <div class="owl-carousel owl-theme owl-responsive-1000" id="home-slider">
                <div class="item">
                    <img src="dist/lib/images/slide/1.png" />
                </div>
                <div class="item">
                    <img src="dist/lib/images/slide/2.png" />
                </div>
                <div class="item">
                    <img src="dist/lib/images/slide/3.png" />
                </div>
            </div>  
        </div>
       
       <!-- End slider header -->
       
       <!-- Strat about section -->
       
       <section class="about">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-6 col-md-12">
                       <div class="about-image">
                           <div class="image"></div>
                       </div>
                   </div>
                   <div class="col-lg-6 col-md-12">
                       <div class="about-content">
                           <div class="title">
                               <h2> about </h2>
                           </div>
                           <div class="content">
                               <p> 
                                   Beyti is one of the largest producers of milk, juice and yoghurt in Egypt, targeting a market of 86 million consumers and catering to different consumer profiles. Beyti was established in 1998 with the acquisition of the largest commercial dairy farm in Egypt from the Saudi Group Dallah
                                    Al-Baraka. Today, Beyti produces a number of agrifoods products, including juices, 100% natural milk, flavored milk, a variety of spoonable and drinkable yoghurts, as well as cooking and whipping creams, for domestic consumption and export sales.
                               </p>
                           </div>
                           <div class="about-video">
                               <div class="title">
                                   <h2> quick tour in beyti factory </h2>
                               </div>
                               <div class="content-video">
                                   <div class="embed-responsive embed-responsive-16by9">
                                   <iframe src="https://www.youtube.com/embed/Eno9Yd3Zd-Q" class="embed-responsive-item" frameborder="0" allowfullscreen=""></iframe>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </section>
       
       <!-- End about section -->
       
       <!-- start OUR BRANDS section -->
       
       <section class="our-brands">
           <div class="container-fluid">
               <div class="title text-center">
                   <h2> Our Barands </h2>
                </div>
               <div class="row">
                   <div class="col-md-12">
                        <div class="owl-carousel owl-theme" id="brands">
                            <div class="item">
                                <img src="dist/lib/images/brands/1.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/2.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/3.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/4.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/5.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/6.png" />
                            </div>
                            <div class="item">
                                <img src="dist/lib/images/brands/7.png" />
                            </div>
                        </div>
                   </div>
               </div>
           </div>
       </section>
       
       <!-- end OUR BRANDS section -->
       
       <!-- Start Products section -->
       
       <section class="products">
           <div class="container">
               <div class="title text-center">
                   <h2> Our products </h2>
                </div>
               <div class="row">
                   <div class="col-md-3">
                       <div class="head"><h5 class="text-center"><a href="">Milk</a></h5></div>
                       <a href="#">
                           <div class="product">
                                <img src="dist/lib/images/products/1.jpg" alt="" title="" />
                           </div>
                       </a>
                   </div>
                   <div class="col-md-3">
                        <div class="head"><h5 class="text-center"><a href="">Juice</a></h5></div>
                        <a href="#">
                            <div class="product">
                                <img src="dist/lib/images/products/2.jpg" alt="" title="" />
                            </div>
                        </a>
                   </div>
                   <div class="col-md-3">
                        <div class="head"><h5 class="text-center"><a href="">Yoghurt</a></h5></div>
                        <a href="#">
                            <div class="product">
                                <img src="dist/lib/images/products/3.jpg" alt="" title="" />
                            </div>
                        </a>
                   </div>
                   <div class="col-md-3">
                        <div class="head"><h5 class="text-center"><a href="">Cream</a></h5></div>
                        <a href="#">
                            <div class="product">
                                <img src="dist/lib/images/products/4.jpg" alt="" title="" />
                            </div>
                        </a>
                   </div>
               </div>
               <div class="button text-center">
                    <a href="#"> See More </a>
               </div>
           </div>
       </section>
       
       <!-- End Products section -->
       
       <!-- Start our news section -->
       
       <section class="our-news">
           <div class="container">
               <div class="title text-center">
                   <h2> Our News </h2>
                </div>
               <div class="row">
                   <div class="col-md-12">
                       <div class="owl-carousel owl-theme" id="news">
                           <div class="item">
                                <div class="our-news-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="news-image">
                                                <a href="#"><img src="dist/lib/images/news/3.jpg" /></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="our-news-details">
                                                <div class="gray-title"><a href="#">our latest news</a></div>
                                                <div class="blue-title">
                                                    <a href="#">Protocol of Cooperation between Beyti and the Largest Egyptian University</a>
                                                </div>
                                                <div class="our-new-text">
                                                    <p>Rabie’ Sayed: Providing the opportunities for the students for practical trainings and opening new horizons for them
                                                    Offering free courses and hosting the students for a month through full  
                                                    </p>

                                                </div>
                                                <div class="see-more">
                                                    <a href="#"> See More </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="item">
                                <div class="our-news-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="news-image">
                                                <a href="#"><img src="dist/lib/images/news/3.jpg" /></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="our-news-details">
                                                <div class="gray-title"><a href="#">our latest news</a></div>
                                                <div class="blue-title">
                                                    <a href="#">Protocol of Cooperation between Beyti and the Largest Egyptian University</a>
                                                </div>
                                                <div class="our-new-text">
                                                    <p>Rabie’ Sayed: Providing the opportunities for the students for practical trainings and opening new horizons for them
                                                    Offering free courses and hosting the students for a month through full  
                                                    </p>

                                                </div>
                                                <div class="see-more">
                                                    <a href="#"> See More </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="item">
                                <div class="our-news-block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="news-image">
                                                <a href="#"><img src="dist/lib/images/news/3.jpg" /></a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="our-news-details">
                                                <div class="gray-title"><a href="#">our latest news</a></div>
                                                <div class="blue-title">
                                                    <a href="#">Protocol of Cooperation between Beyti and the Largest Egyptian University</a>
                                                </div>
                                                <div class="our-new-text">
                                                    <p>Rabie’ Sayed: Providing the opportunities for the students for practical trainings and opening new horizons for them
                                                    Offering free courses and hosting the students for a month through full  
                                                    </p>

                                                </div>
                                                <div class="see-more">
                                                    <a href="#"> See More </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           
                        </div>
                   </div>
               </div>
               
           </div>
       </section>
       
       <!-- End our news section -->

       <!-- Start our news second -->
        <section class="our-news-second">
            <div class="container">
                <div class="row">
                   <div class="col-md-12">
                       <div class="owl-carousel owl-theme" id="sec-news">
                            <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="item">
                                <a href="#">
                                    <div class="sec-news-block">
                                        <div class="img-contain">
                                            <img src="dist/lib/images/news/5.png" />
                                        </div>
                                        <div class="text">
                                            <p>Beyti organizes training courses concerning risk analysis for the students of veterinary medicine</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                   </div>
               </div>               
            </div>
        </section>

       <!-- /End our news second -->
       
        <!-- Start Gallery Section -->
        <section class="gallery">
          <div class="container">
             <div class="row">
                <div class="col-lg-5 col-md-6 col-12">
                      <img src="dist/lib/images/1.png" class="gal-img">
                      <div id="gallery-video" class="owl-carousel owl-theme">
                         <div>
                             <a href="#">
                             <img src="dist/lib/images/134ce63057f068a219a0df338fb0b723-5620900-9421519.jpg" class="img-fluid">
                             <div class="carousel-title">
                                <span>Om Aly</span>
                             </div> 
                                 </a>  
                         </div>
                         <div>
                             <a href="#">
                             <img src="dist/lib/images/030d7e8e966169ab4c7f67c291c333f4-5278478-6038633.jpg" class="img-fluid">
                             <div class="carousel-title">
                                 <span>Eggplant Fatteh</span>
                             </div>
                             </a>
                         </div>
                         <div>
                              <a href="#">
                             <img src="dist/lib/images/ca538c343179bf0fbdfab6cd10469afd-643449-3231615.jpg" class="img-fluid">
                             <div class="carousel-title">
                               <span>Potato Gratin</span>
                             </div>
                                  </a>
                         </div>
                      </div>  
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                     <div id="video-slider2" class="owl-carousel owl-theme">
                       <div class="item-video" data-merge="3"><a class="owl-video" href="https://www.youtube.com/watch?v=u3sAPPz8EcI"></a></div>
                       <div class="item-video" data-merge="1"><a class="owl-video" href="https://www.youtube.com/watch?v=u3sAPPz8EcI"></a></div>
                       <div class="item-video" data-merge="2"><a class="owl-video" href="https://www.youtube.com/watch?v=u3sAPPz8EcI"></a></div>
                       <div class="item-video" data-merge="1"><a class="owl-video" href="https://www.youtube.com/watch?v=u3sAPPz8EcI"></a></div>
                       
                   </div>
                 
                </div>  
             </div>
          </div>
      </section>
         <!-- End Gallery Section -->

       <!-- Start vote section -->
       
       <section class="vote">
           <div class="container">
               <div class="title text-center">
                   <h2> Vote For Your Best Flavor From Beyti Tropicana </h2>
                </div>
               <div class="row">
                   <div class="col-md-12">
                       <div class="owl-carousel owl-theme" id="vote">
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/1.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/2.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/3.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/4.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/1.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/2.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                           <div class="item">
                                <div class="vote-block">
                                    <img src="dist/lib/images/vote/3.jpg" />
                                    <div class="vote-rate">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                   </div>
               </div>
           </div>
       </section>
       
       <!-- End vote section -->

       <?php include "include/footer.php" ?>
     