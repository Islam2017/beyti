 <?php include "include/header.php" ?>
       
   <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="head">
                            <h2> About Us </h2>
                            <h2 class="span"> WHO WE ARE </h2>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="img">
                                    <img src="dist/lib/images/ico2.png" />
                                    <p>3000+ Employees</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img">
                                    <img src="dist/lib/images/ico1.png" />
                                    <p>4 Product Categories</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="img">
                                    <img src="dist/lib/images/ico3.png" />
                                    <p>1998 Established</p>
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <p> Beyti is one of the largest producers of milk, juice and yoghurt in Egypt, targeting a market of 86 million consumers and catering to different consumer profiles. Beyti was established in 1998 with the acquisition of the largest commercial dairy farm in Egypt from the Saudi Group Dallah
                            Al-Baraka. Today, Beyti produces a number of agrifoods products, including juices, 100% natural milk, flavored milk, a variety of spoonable and drinkable yoghurts, as well as cooking and whipping creams, for domestic consumption and export sales. 
                            </p>
                            <p> Beyti is one of the largest producers of milk, juice and yoghurt in Egypt, targeting a market of 86 million consumers and catering to different consumer profiles. Beyti was established in 1998 with the acquisition of the largest commercial dairy farm in Egypt from the Saudi Group Dallah
                            Al-Baraka. Today, Beyti produces a number of agrifoods products, including juices, 100% natural milk, flavored milk, a variety of spoonable and drinkable yoghurts, as well as cooking and whipping creams, for domestic consumption and export sales. 
                            </p>
                            <p> Beyti is one of the largest producers of milk, juice and yoghurt in Egypt, targeting a market of 86 million consumers and catering to different consumer profiles. Beyti was established in 1998 with the acquisition of the largest commercial dairy farm in Egypt from the Saudi Group Dallah
                            Al-Baraka. Today, Beyti produces a number of agrifoods products, including juices, 100% natural milk, flavored milk, a variety of spoonable and drinkable yoghurts, as well as cooking and whipping creams, for domestic consumption and export sales. 
                            </p>
                            <p> Beyti is one of the largest producers of milk, juice and yoghurt in Egypt, targeting a market of 86 million consumers and catering to different consumer profiles. Beyti was established in 1998 with the acquisition of the largest commercial dairy farm in Egypt from the Saudi Group Dallah
                            Al-Baraka. Today, Beyti produces a number of agrifoods products, including juices, 100% natural milk, flavored milk, a variety of spoonable and drinkable yoghurts, as well as cooking and whipping creams, for domestic consumption and export sales. 
                            </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                             <div class="head">
                                 <h2> VOTE FOR THE BEST FLAVOR </h2>
                            </div>
                            <div class="slider">
                                <div class="owl-carousel owl-theme" id="side-vote">
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/4.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>              
                            </div>
                            <div class="main-block">
                                <div class="block_">
                                    <a href="description.php">
                                        <img class="" src="dist/lib/images/1.png" />
                                        <p>  </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/2.jpg" />
                                        <p> Om Aly </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/3.jpg" />
                                        <p> Eggplant Fatteh </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/4.jpg" />
                                        <p> Potato Gratin </p>
                                    </a>
                                </div>
                                <a href="almaria.php" class="see"> See More </a>
                                <div class="other-block">
                                    <a href="#">
                                        <img src="dist/lib/images/job.png" />
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>