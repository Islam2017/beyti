 <?php include "include/header.php" ?>
       
   <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="sidebar card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="list-group-item active-menu">
                                    <a href="#">My Profile</a>
                                </li>
                                <li class="list-group-item">
                                    <a class="text-danger" href="#">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="item-main">
                        <form>
                            <div class="card">
                                <div class="card-header">
                                Personal Information
                                </div>
                                <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Full Name</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" placeholder="Ahmed Mohamed Ali">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Birth Date</label>
                                            <div class="col-sm-2">
                                                <select class="custom-select col-sm-12">
                                                    <option value="1">25</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="custom-select col-sm-12">
                                                    <option value="1">May</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <select class="custom-select col-sm-12">
                                                    <option value="1">1985</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Nationality</label>
                                            <div class="col-sm-9">
                                                <select class="custom-select">
                                                    <option value="1">Egyptian</option>
                                                </select>
                                            </div>
                                        </div>

                                        <fieldset class="form-group">
                                            <div class="row">
                                                <label class="col-form-label col-sm-3 pt-0">Gender</label>
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input class="custom-control-input" type="radio" name="gender" id="gender1" value="option1" checked>
                                                        <span class="custom-control-indicator"></span>
                                                        <label class="form-check-label" for="gender1">
                                                            Male
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="custom-control-input" type="radio" name="gender" id="gender2" value="option1">
                                                        <span class="custom-control-indicator"></span>
                                                        <label class="form-check-label" for="gender2">
                                                            Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset class="form-group">
                                            <div class="row">
                                                <label class="col-form-label col-sm-3 pt-0">Marital Status</label>
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input class="custom-control-input" type="radio" name="MaritalStatus" id="MaritalStatus1" value="option1" checked>
                                                        <span class="custom-control-indicator"></span> 
                                                        <label class="form-check-label" for="MaritalStatus1">
                                                            Unspecified
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="custom-control-input" type="radio" name="MaritalStatus" id="MaritalStatus2" value="option1">
                                                        <span class="custom-control-indicator"></span>
                                                        <label class="form-check-label" for="MaritalStatus2">
                                                            Single
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="custom-control-input" type="radio" name="MaritalStatus" id="MaritalStatus3" value="option1">
                                                        <span class="custom-control-indicator"></span>
                                                        <label class="form-check-label" for="MaritalStatus3">
                                                            Married
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Driving License</label>
                                            <div class="col-sm-9">
                                                <select class="custom-select">
                                                    <option value="1">First degree</option>
                                                    <option value="1">Second degree</option>
                                                    <option value="1">Third degree</option>
                                                    <option value="1">Private</option>
                                                    <option value="1">No Licenses</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card">
                                    <div class="card-header">
                                    Contact Detials
                                    </div>
                                    <div class="card-body">
                                
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Address</label>
                                            <div class="col-sm-9">Personal Information
                                                <input type="text" class="form-control" placeholder="53 Misr st, Maadi, Cairo">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">City</label>
                                            <div class="col-sm-3">
                                                <select class="custom-select col-sm-12">
                                                    <option value="1">Maadi</option>
                                                </select>
                                            </div>
                                            
                                            <label class="col-sm-2" for="formGroupExampleInput">Governorate</label>
                                            <div class="col-sm-4">
                                                <select class="custom-select col-sm-12">
                                                    <option value="1">Cairo</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Contact Number</label>
                                            <div class="col-sm-9">
                                                <input type="tel" class="form-control" placeholder="010********">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Email Address</label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" placeholder="user@domain.com">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card">
                                    <div class="card-header">
                                    Attachments
                                    </div>
                                    <div class="card-body">
                                    
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">National ID</label>
                                            <div class="col-sm-9">
                                                <label class="custom-file">
                                                    <input type="file" id="file" class="custom-file-input">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Personal photo</label>
                                            <div class="col-sm-9">
                                                <label class="custom-file">
                                                    <input type="file" id="file" class="custom-file-input">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">Driving License</label>
                                            <div class="col-sm-9">
                                                <label class="custom-file">
                                                    <input type="file" id="file" class="custom-file-input">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3" for="formGroupExampleInput">C.V</label>
                                            <div class="col-sm-9">
                                                <label class="custom-file">
                                                    <input type="file" id="file" class="custom-file-input">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Change</button>
                        </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>