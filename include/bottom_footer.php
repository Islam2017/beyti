 <!-- Start section  Footer -->
       <div class="clearfix"></div>
       <section class="footer">
           <div class="bottom-footer">
               <div class="container">
                   <div class="row">
                       <div class="col-md-6">
                           <p>Copyrights ©2017 IDJ, All Rights Reserved - Beyti Egypt</p>
                       </div>
                       <div class="col-md-6">
                           <div class="links">
                               <a href="#"> Find Us </a>
                               <a href="#" data-toggle="modal" data-target="#facebook"><i class="fa fa-facebook"></i></a>
                                <!-- Modal -->
                                <div class="modal fade" id="facebook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Facebook Pages</h5>
                                        <div class="modal-logo">
                                            <img class="img-responsive" src="dist/lib/images/logo.png">
                                        </div>
                                      </div>
                                      <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                             <!-- normal -->
                                            <div class="ih-item circle effect1">
                                                <a href="#">
                                                    <div class="spinner"></div>
                                                    <div class="img">
                                                        <img src="dist/lib/images/111.png" alt="img">
                                                    </div>
                                                    <div class="info">
                                                      <div class="info-back">
                                                        <h3>Al Marai Egypt </h3>
                                                      </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <!-- end normal -->
                                            </div>
                                             <div class="col-md-2 col-sm-4 col-xs-6">
                                                 <!-- normal -->
                                                <div class="ih-item circle effect1">
                                                    <a href="#">
                                                        <div class="spinner"></div>
                                                        <div class="img">
                                                            <img src="dist/lib/images/222.png" alt="img">
                                                        </div>
                                                        <div class="info">
                                                          <div class="info-back">
                                                            <h3>Beyti Corporate </h3>
                                                          </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <!-- end normal -->
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                 <!-- normal -->
                                                <div class="ih-item circle effect1">
                                                    <a href="#">
                                                        <div class="spinner"></div>
                                                        <div class="img">
                                                            <img src="dist/lib/images/333.jpg" alt="img">
                                                        </div>
                                                        <div class="info">
                                                          <div class="info-back">
                                                            <h3>Beyti Juice</h3>
                                                          </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <!-- end normal -->
                                            </div>
                                            <div class="col-md-2 col-sm-4 col-xs-6">
                                                 <!-- normal -->
                                                <div class="ih-item circle effect1">
                                                    <a href="#">
                                                        <div class="spinner"></div>
                                                        <div class="img">
                                                            <img src="dist/lib/images/444.png" alt="img">
                                                        </div>
                                                        <div class="info">
                                                          <div class="info-back">
                                                            <h3>Beyti Milk</h3>
                                                          </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <!-- end normal -->
                                            </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                               <a href="#"><i class="fa fa-instagram"></i></a>
                           </div>  
                       </div>
                   </div>
               </div>
           </div>
           <div class="scroll-top-wrapper">
                <a href="#"><i class="fa fa-angle-up"></i></a>
           </div>
       </section>
       
       <!-- End section  Footer -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="dist/lib/js/jquery-3.2.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
      <script src="dist/lib/js/bootstrap.min.js"></script>
      <!-- owl carousel javascript -->
      <script src="dist/lib/js/owl.carousel.min.js"></script>
      <!-- fancybox javascript -->
      <script src="dist/lib/js/jquery.fancybox.min.js"></script>   
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="dist/lib/js/custom.js"></script> 
      <script>
         (function() {
             'use strict'
         
             if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                 var msViewportStyle = document.createElement('style')
                 msViewportStyle.appendChild(
                     document.createTextNode(
                         '@-ms-viewport{width:auto!important}'
                     )
                 )
                 document.head.appendChild(msViewportStyle)
             }
            $('#myTab a').on('click', function (e) {
                e.preventDefault()
                $(this).tab('show')
            })

         }())
         
         
      </script>
   </body>
</html>