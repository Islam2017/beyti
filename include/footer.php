 <!-- Start section  Footer -->
       <div class="clearfix"></div>
       <section class="footer">
           <div class="home-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.6173416961874!2d31.407625485391815!3d30.01914218189152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583cf75280a00d%3A0xe8a202780043b5ee!2sBeyti+-+Al+Marai+Egypt!5e0!3m2!1sar!2seg!4v1502621612599" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
            <div class="map-info">
                <h2> Contact Us</h2>
                <p>Corporate Offices
                    <strong>Head Office</strong>
                    Cairo Festival City, Building A2, 1st Floor, New Cairo 5th Settlement, Cairo, Egypt Short number 16964 Mobile: 010-24631760 / 010-24631788 Landline: +(202) 23220491-9 (9 Lines)            </p>
                <h2 class="m-t-90">Find us on social media </h2>
                <div class="social-links">
                    <a href="#" data-toggle="modal" data-target="#facebook"> 
                      Facebook  <i class="fa fa-facebook"></i>
                    </a>
                    <!-- Modal -->
                    <div class="modal fade" id="facebook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Facebook Pages</h5>
                            <div class="modal-logo">
                                <img class="img-responsive" src="dist/lib/images/logo.png">
                            </div>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                 <!-- normal -->
                                <div class="ih-item circle effect1">
                                    <a href="#">
                                        <div class="spinner"></div>
                                        <div class="img">
                                            <img src="dist/lib/images/111.png" alt="img">
                                        </div>
                                        <div class="info">
                                          <div class="info-back">
                                            <h3>Al Marai Egypt </h3>
                                          </div>
                                        </div>
                                    </a>
                                </div>
                                <!-- end normal -->
                                </div>
                                 <div class="col-md-2 col-sm-4 col-xs-6">
                                     <!-- normal -->
                                    <div class="ih-item circle effect1">
                                        <a href="#">
                                            <div class="spinner"></div>
                                            <div class="img">
                                                <img src="dist/lib/images/222.png" alt="img">
                                            </div>
                                            <div class="info">
                                              <div class="info-back">
                                                <h3>Beyti Corporate </h3>
                                              </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- end normal -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                     <!-- normal -->
                                    <div class="ih-item circle effect1">
                                        <a href="#">
                                            <div class="spinner"></div>
                                            <div class="img">
                                                <img src="dist/lib/images/333.jpg" alt="img">
                                            </div>
                                            <div class="info">
                                              <div class="info-back">
                                                <h3>Beyti Juice</h3>
                                              </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- end normal -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                     <!-- normal -->
                                    <div class="ih-item circle effect1">
                                        <a href="#">
                                            <div class="spinner"></div>
                                            <div class="img">
                                                <img src="dist/lib/images/444.png" alt="img">
                                            </div>
                                            <div class="info">
                                              <div class="info-back">
                                                <h3>Beyti Milk</h3>
                                              </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- end normal -->
                                </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <a href="#">Instagram
                        <i class="fa fa-instagram"></i>
                    </a>
                </div>
            </div>
            </div>
           <div class="bottom-footer">
               <div class="container">
                   <div class="row">
                       <div class="col-md-6">
                           <p>Copyrights ©2017 IDJ, All Rights Reserved - Beyti Egypt</p>
                       </div>
                       <div class="col-md-6">
                           <div class="links">
                               <a href="#"> Find Us </a>
                               <a href="#" data-toggle="modal" data-target="#facebook"><i class="fa fa-facebook"></i></a>
                               <a href="#"><i class="fa fa-instagram"></i></a>
                           </div>  
                       </div>
                   </div>
               </div>
           </div>
           <div class="scroll-top-wrapper">
                <a href="#"><i class="fa fa-angle-up"></i></a>
           </div>
       </section>
       
       <!-- End section  Footer -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="dist/lib/js/jquery-3.2.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
      <script src="dist/lib/js/bootstrap.min.js"></script>
      <!-- owl carousel javascript -->
      <script src="dist/lib/js/owl.carousel.min.js"></script>
      <!-- fancybox javascript -->
      <script src="dist/lib/js/jquery.fancybox.min.js"></script>   
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="dist/lib/js/custom.js"></script> 
      <script>
         (function() {
             'use strict'
         
             if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                 var msViewportStyle = document.createElement('style')
                 msViewportStyle.appendChild(
                     document.createTextNode(
                         '@-ms-viewport{width:auto!important}'
                     )
                 )
                 document.head.appendChild(msViewportStyle)
             }
         }())
         
         
      </script>
   </body>
</html>