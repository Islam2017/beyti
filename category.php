 <?php include "include/header.php" ?>
       
   <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                      <div class="item-main">
                        <div class="item-post">
                            <h1 class="item-post-title">Yoghurt</h1>
                            <p class="item-post-meta">
                                We first introduced our line of spoonable yoghurts in 2007. Since then, we have diversified into two brands — Almarai and Beyti — with a total of 16 products, ranging from classic plain spoonable yoghurt to drinking yoghurt, rayeb and individual serving-sized cups of fruit-on-the-bottom spoonable yoghurts.
                                With one of the most trusted and popular lines of spoonable and drinkable yoghurt in Egypt, our yoghurts are produced to the same high standards and using the same first-class materials as our fresh milk products, ensuring quality and helping us to keep costs low in this competitive market. Milk for our yoghurt products is sourced from long-term, trusted partners, allowing us maximum control of the supply chain and the quality of our products. We use only the very best packaging for our yoghurts, which abides by the global standard in hygienic, safe and environmentally-sound packaging, ensuring that our consumers can count on us for high-quality yoghurt.
                                You can browse through our various yoghurt product offerings on the right. 
                            </p>     
                         </div>
                            <div class="owl-carousel owl-theme" id="vote" style="margin-top:50px;">
                               <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="vote-block">
                                        <img src="dist/lib/images/vote/1.jpg" />
                                         Almarai Yoghurt 105ml
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                             <div class="head">
                                 <h2> VOTE FOR THE BEST FLAVOR </h2>
                            </div>
                            <div class="slider">
                                <div class="owl-carousel owl-theme" id="side-vote">
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/4.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>              
                            </div>
                            <div class="main-block">
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/1.png" />
                                        <p>  </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/2.jpg" />
                                        <p> Om Aly </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/3.jpg" />
                                        <p> Eggplant Fatteh </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/4.jpg" />
                                        <p> Potato Gratin </p>
                                    </a>
                                </div>
                                <a href="almaria.php" class="see"> See More </a>
                                <div class="other-block">
                                    <a href="#">
                                        <img src="dist/lib/images/job.png" />
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>