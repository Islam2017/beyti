 <?php include "include/header.php" ?>
       

    <!-- Start about content -->
        <section class="login-page">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-6">
                        <div class="item-main">
                        <form>
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-fill login-nav" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#login">Login</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#register">Register</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#rest">Rest Password</a>
                                        </li>
                                    </ul>
                                    <br><br>
                                    <div class="tab-content">
                                        <div class="tab-pane active" role="tabpanel" aria-labelledby="register-tab" id="login">
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" placeholder="email@example.com">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label" for="formGroupExampleInput">Password</label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" placeholder="*******">
                                                </div>
                                            </div>


                                            <div class="form-check">
                                                <input class="custom-control-input" type="checkbox" name="rememberME" id="rememberME" value="1" checked>
                                                <span class="custom-control-indicator"></span> 
                                                <label class="form-check-label" for="MaritalStatus1">
                                                    Remember me!
                                                </label>
                                            </div>
                                            <button type="submit" class="btn btn-outline-primary btn-block">Login</button>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" aria-labelledby="register-tab" id="register">
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                                                <div class="col-sm-8">
                                                    <input type="email" class="form-control" value="email@example.com">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="formGroupExampleInput">Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" class="form-control" placeholder="*******">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="formGroupExampleInput">Confirm Password</label>
                                                <div class="col-sm-8">
                                                    <input type="password" class="form-control" placeholder="*******">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-outline-primary btn-block">Register</button>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" aria-labelledby="rest-tab" id="rest">

                                            <div class="alert alert-success" role="alert">
                                                Reset Email has been sent to your inbox!
                                            </div>
                                            <div class="alert alert-danger" role="alert">
                                                Your Email Address can't be found!
                                            </div>
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" placeholder="email@example.com">
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-outline-primary btn-block">Rest Password</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>