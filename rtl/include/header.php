<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="">
      <title>Starter Byti</title>
      <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="dist/lib/css/bootstrap-rtl.css">
      <!-- owl carousel stylesheet -->
      <link rel="stylesheet" href="dist/lib/css/owl.carousel.min.css">
      <link rel="stylesheet" href="dist/lib/css/owl.theme.default.css">
      <!-- fancybox stylesheet -->
      <link rel="stylesheet" href="dist/lib/css/jquery.fancybox.min.css">
      <!-- font-awesome stylesheet -->
      <link rel="stylesheet" href="dist/lib/css/font-awesome.min.css">
    
      <!-- custom stylesheet -->
       <link rel="stylesheet" href="dist/lib/css/main.css">
      <link rel="stylesheet" href="dist/lib/css/custom_rtl.css">
      <!-- Custom styles for this template -->
   </head>
   <body>
       
       <!-- Start Header  -->
       
       <header class="home-header">
           <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container">
                    <a class="navbar-brand" href="#">
                        <img class="img-responsive" src="dist/lib/images/logo.png" alt="Byti Egypt" title="Byti Egypt" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbar">
                      <ul class="navbar-nav navbar-left mr-auto p-0">
                          <li class="nav-item active">
                            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="about.php">About Us</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="almaria.php">Almarai KITCHEN</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="news.php">News</a>
                          </li>
                      </ul>
                      <ul class="navbar-nav navbar-right ml-auto p-0">
                          <li class="nav-item">
                            <a class="nav-link" href="media.php">Media</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="product.php">Products</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="https://careers.beytiegypt.com/ar/">Careers</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="contact.php">Contacts</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">ع</a>
                          </li>
                      </ul>
                    </div>
                </div>  
           </nav>
       </header>
       
       <!-- End Header  -->