 <!-- Start section  Footer -->
       <div class="clearfix"></div>
       <section class="footer">
           <div class="bottom-footer">
               <div class="container">
                   <div class="row">
                       <div class="col-md-6">
                           <p>Copyrights ©2017 IDJ, All Rights Reserved - Beyti Egypt</p>
                       </div>
                       <div class="col-md-6">
                           <div class="links">
                               <a href="#"> Find Us </a>
                               <a href="#"><i class="fa fa-facebook"></i></a>
                               <a href="#"><i class="fa fa-instagram"></i></a>
                           </div>  
                       </div>
                   </div>
               </div>
           </div>
           <div class="scroll-top-wrapper">
                <a href="#"><i class="fa fa-angle-up"></i></a>
           </div>
       </section>
       
       <!-- End section  Footer -->
      <!-- Bootstrap core JavaScript
         ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="dist/lib/js/jquery-3.2.0.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
      <script src="dist/lib/js/bootstrap.min.js"></script>
      <!-- owl carousel javascript -->
      <script src="dist/lib/js/owl.carousel.min.js"></script>
      <!-- fancybox javascript -->
      <script src="dist/lib/js/jquery.fancybox.min.js"></script>   
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="dist/lib/js/custom_rtl.js"></script> 
      <script>
         (function() {
             'use strict'
         
             if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                 var msViewportStyle = document.createElement('style')
                 msViewportStyle.appendChild(
                     document.createTextNode(
                         '@-ms-viewport{width:auto!important}'
                     )
                 )
                 document.head.appendChild(msViewportStyle)
             }
            $('#myTab a').on('click', function (e) {
                e.preventDefault()
                $(this).tab('show')
            })

         }())
         
         
      </script>
   </body>
</html>