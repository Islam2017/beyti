 <?php include "include/header.php" ?>
    <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_ media">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Beyti Produces a broad range of natural and healthy dairy and juice products across four distinct product lines, including: Milk, Juice, Yoghurt and Creams. You can flip through our product offerings by category, below, or browse to each product line page for more information.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <h2><a href=""> Milk </a></h2>
                                <div class="sidebar">
                                    <div class="slider">
                                        <div class="side-vote-prod owl-carousel owl-theme" id="">
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/4.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h2><a href=""> Juice </a></h2>
                                <div class="sidebar">
                                    <div class="slider">
                                        <div class="side-vote-prod owl-carousel owl-theme" id="">
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/4.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h2><a href=""> Yoghurt </a></h2>
                                <div class="sidebar">
                                    <div class="slider">
                                        <div class="side-vote-prod owl-carousel owl-theme" id="">
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/4.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>    
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h2><a href=""> Cream </a></h2>
                                <div class="sidebar">
                                    <div class="slider">
                                        <div class="side-vote-prod owl-carousel owl-theme" id="">
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/4.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/1.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/2.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="item">
                                                <div class="vote-block">
                                                    <img src="dist/lib/images/vote/3.jpg" />
                                                    <div class="vote-rate">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>              
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>