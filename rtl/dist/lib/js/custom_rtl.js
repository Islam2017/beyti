// start 

$(document).ready(function(){
    
    // navbar
    
    $(".navbar-toggler").click(function(){
        
         if ($('#navbar').attr('aria-expanded') === "true") {
                $( ".bread-crumb-style" ).show();
                $(".white-container").css({"padding-top": "65px" , "-moz-transition": "all 0.4s" , "-webkit-transition" : "all 0.4s" , "transition" : "all 0.4s"});
                
            }
            else{
                 $( ".bread-crumb-style" ).hide();
                 $(".white-container").css({"padding-top": "170px" , "-moz-transition": "all 0.4s" , "-webkit-transition" : "all 0.4s" , "transition" : "all 0.4s"});
            }
        });
    
        $(window).scroll(function() {
          if ($(document).scrollTop() > 50) {
            $('.bg-light').addClass('shrink');
          } else {
            $('.bg-light').removeClass('shrink');
          }
        });
    
    
        // slider header
        $('#home-slider').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            rtl:true,
            autoplay:true,
            nav:true,
            dots:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            items: 1,
            smartSpeed:1000,
            responsive:{

                1200:{
                    items:1
                }
            },
        })
    
    // slider brands
    
    $('#brands').owlCarousel({
            loop:true,
            margin:20,
            nav:true,
            autoplay:true,
            nav:true,
            dots:true,
            rtl:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            smartSpeed:1000,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:3,
                    nav:false
                },            
                960:{
                    items:5
                },
                1200:{
                    items:7
                }
            },
        })
    
    // slider news
        $('#news').owlCarousel({
            loop:true,
            margin:0,
            nav:false,
            rtl:true,
            autoplay:true,
            dots:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            items: 1,
            smartSpeed:1000,
            responsive:{
                1200:{
                    items:1
                }
            },
        })
    
    // slider sec-news
        $('#sec-news').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            rtl:true,
            autoplay:true,
            dots:false,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            smartSpeed:1000,
            responsive:{
               0:{
                    items:1,
                   nav:false,
                },
                600:{
                    items:2,
                    nav:false,
                },            
                960:{
                    items:3
                },
                1200:{
                    items:4
                }
            },
        })
    
    // slider vote
    
    // slider sec-news
        $('#vote').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:true,
            dots:true,
            rtl:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            smartSpeed:1000,
            responsive:{
               0:{
                    items:1,
                    nav:false,
                },
                600:{
                    items:2,
                    nav:false
                },            
                960:{
                    items:3
                },
                1200:{
                    items:4
                }
            },
        })
    
    // slider sidebar vote
    
    // slider news
        $('#side-vote').owlCarousel({
            loop:true,
            margin:0,
            nav:true,
            autoplay:true,
            dots:false,
            rtl:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            items: 1,
            smartSpeed:1000,
            responsive:{
                1200:{
                    items:1
                }
            },
        })
    
    // slider almaria
        $('#almaria').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:true,
            dots:false,
            rtl:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            smartSpeed:1000,
            responsive:{
               0:{
                    items:1,
                   nav:false,
                },
                600:{
                    items:2,
                    nav:false,
                },            
                960:{
                    items:3
                },
                1200:{
                    items:4
                }
            },
        })
    
    
    // slider almaria
        $('.side-vote-prod').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay:true,
            dots:false,
            rtl:true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsiveClass:true,
            smartSpeed:1000,
            responsive:{     
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:1,
                    nav:false
                },            
                960:{
                    items:1
                },
                1200:{
                    items:1
                }
            },
        })
    
    // gallery video slider
       $('#gallery-video').owlCarousel({
           loop:true,
           margin:0,
           nav:true,
           autoplay:true,                    
           dots:false,
           rtl:true,
           navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
           responsiveClass:true,
           items: 1,
           smartSpeed:1000
       });
       $('#video-slider2').owlCarousel({
               items:1,
               merge:true,
               loop:true,
               margin:0,
               rtl:true,
               video:true,
               videoWidth: 360,
               videoHeight: 300,
               center:true
           });
    // scroll top 
    
     $(function(){
 
        $(document).on( 'scroll', function(){

            if ($(window).scrollTop() > 100) {
                $('.scroll-top-wrapper').addClass('show');
            } else {
                $('.scroll-top-wrapper').removeClass('show');
            }
        });
 
	       $('.scroll-top-wrapper').on('click', scrollToTop);
        });
 
        function scrollToTop() {
            verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
            element = $('body');
            offset = element.offset();
            offsetTop = offset.top;
            $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
        }
    
});