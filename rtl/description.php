 <?php include "include/header.php" ?>
       
   <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="item-main">
                            <div class="item-post">
                                <h1 class="item-post-title">Om Aly</h1>
                                <p class="item-post-meta">
                                   <small>0000-00-00</small>
                                   <br>
                                </p>
                                <p><strong>Ingredients </strong></p>
                                <ul>
                                    <li>1 liter Almarai milk</li>
                                    <li>2 spoons of sugar</li>
                                    <li>A pack of Om Ali from the supermarket</li>
                                    <li>250ml of Almarai whipped cream</li>
                                    <li>4 spoons of butter</li>
                                </ul>
                                <p><strong>The Recipe </strong></p>
                                <ul>
                                    <li>Heat the sugar and milk</li>
                                    <li>Mix Almarai whipped cream with the electric blender</li>
                                    <li>Put the Om Ali in an oven pot, add milk, butter, and the cream at the end</li>
                                    <li>Put it in the oven for 10 minutes</li>
                                    <li>Add your most favorite nuts.&nbsp;</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                             <div class="head">
                                 <h2> VOTE FOR THE BEST FLAVOR </h2>
                            </div>
                            <div class="slider">
                                <div class="owl-carousel owl-theme" id="side-vote">
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/4.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/1.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/2.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="item">
                                        <div class="vote-block">
                                            <img src="dist/lib/images/vote/3.jpg" />
                                            <div class="vote-rate">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>              
                            </div>
                            <div class="main-block">
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/1.png" />
                                        <p>  </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/2.jpg" />
                                        <p> Om Aly </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/3.jpg" />
                                        <p> Eggplant Fatteh </p>
                                    </a>
                                </div>
                                <div class="block">
                                    <a href="description.php">
                                        <img src="dist/lib/images/4.jpg" />
                                        <p> Potato Gratin </p>
                                    </a>
                                </div>
                                <a href="almaria.php" class="see"> See More </a>
                                <div class="other-block">
                                    <a href="#">
                                        <img src="dist/lib/images/job.png" />
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>