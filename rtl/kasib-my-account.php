 <?php include "include/header.php" ?>
       
   <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="sidebar card">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item active-menu">
                                    <a href="#">My Account</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">My Profile</a>
                                </li>
                                <li class="list-group-item">
                                    <a class="text-danger" href="#">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="item-main">
                        <form>
                            <div class="card">
                                <div class="card-header">
                                    My Account
                                </div>
                                <div class="card-body">
                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                                            <div class="col-sm-9">
                                                <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label" for="formGroupExampleInput">Old password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" placeholder="*******">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label" for="formGroupExampleInput">New password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" placeholder="*******">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label" for="formGroupExampleInput">Confirm new password</label>
                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" placeholder="*******">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Change</button>
                                    
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/bottom_footer.php" ?>