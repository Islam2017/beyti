 <?php include "include/header.php" ?>
       
 <!-- Start banner -->
        <section class="banner" style="background-image: url(dist/lib/images/aboutus_home.jpg);"></section>
   <!-- Start banner -->

    <!-- Start about content -->
        <section class="about-content_ contact">
            <div class="container">
                <div class="row">
                   <div class="col-md-6">
                        <div class="head-contact">
                            <h2>Corporate Offices</h2>
                        </div>
                       <div class="content-contact">
                           <strong>Head Office</strong>
                           <p>Cairo Festival City, Building A2, 1st Floor, New Cairo 5th Settlement, Cairo, Egypt Short number 16964 Mobile: 010-24631760 / 010-24631788 Landline: +(202) 23220491-9 (9 Lines)</p>
                       </div>
                        <div class="head-contact">
                            <h2>Eportes</h2>
                        </div>
                       <div class="content-contact">
                           <p>Export@Beyti-IDJ.com</p>
                       </div>
                    </div>
                    <div class="col-md-6">
                        <div class="head-contact">
                            <h2>Manufacturing Facility</h2>
                        </div>
                       <div class="content-contact">
                           <strong>Head Office</strong>
                           <p>Cairo Festival City, Building A2, 1st Floor, New Cairo 5th Settlement, Cairo, Egypt Short number 16964 Mobile: 010-24631760 / 010-24631788 Landline: +(202) 23220491-9 (9 Lines)</p>
                       </div>
                        <div class="head-contact">
                            <h2>Consumer Complaints and Corporate Email</h2>
                        </div>
                       <div class="content-contact">
                           <p>Export@Beyti-IDJ.com</p>
                       </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="contact-form">
            <div class="container">
                 <div class="head-contact">
                    <h2>Feel Free To Contact Us Or Call Us 16964</h2>
                 </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="" class="form-control" placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="" class="form-control" placeholder="Email" />
                                        </div>
                                    </div>        
                                </div>
                                <div class="form-group">
                                    <input type="text" name="" class="form-control" placeholder="Phone" />
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" name="" placeholder="Message"></textarea>
                                </div>
                                <div class="form-group text-center">
                                    <input type="button" name="" class="btn btn-primary" value="Submit" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- Start about content -->

 <?php include "include/footer.php" ?>